require 'json'
require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/namespace'
require './models/location'
require './models/user'
require './models/item'
require './models/order'

ORDER_PAYED = 'Pagado'.freeze
ORDER_DELIVERED = 'Entregado'.freeze
ORDER_FINISHED = 'Finalizado'.freeze

# Main class of the application
class DeliveruApp < Sinatra::Application
  register Sinatra::Namespace

  enable :sessions unless test?

  configure :development do
    pid = begin
            File.read('./node.pid')
          rescue StandardError
            nil
          end

    if pid.nil?
      ## Start the node server to run React
      pid = Process.spawn('npm run dev')
      Process.detach(pid)
      File.write('./node.pid', pid.to_s)
    end
  end
  
  ## Function to clean up the json requests.
  before do
    begin
      if request.body.read(1)
        request.body.rewind
        @request_payload = JSON.parse(request.body.read, symbolize_names: true)
      end
    rescue JSON::ParserError
      request.body.rewind
      puts "The body #{request.body.read} was not JSON"
    end
  end

  register do
    def auth
      condition do
        halt 401 unless session.key?(:logged_id) || self.settings.test?
      end
    end
  end

  ## API functions
  namespace '/api' do
    # TODO program API get and post calls.

    post '/consumers' do
      email = @request_payload[:email]
      location = @request_payload[:location]
      password = @request_payload[:password]
      consumer_hash = @request_payload.clone
      consumer_hash.store("id", Consumer.max_id)
      consumer_hash.store("balance", 0)
      if email.nil? || location.nil?
        halt 400  ## Campos necesarios
      end
      if Consumer.exists?({'email'=>email})
        halt 409  ## El usuario ya existe
      end
      consumer = Consumer.from_hash(consumer_hash)
      consumer.update
      consumer.id.to_json
    end

    post '/providers' do
      email = @request_payload[:email]
      store_name = @request_payload[:store_name]
      location = @request_payload[:location]
      password = @request_payload[:password]
      max_delivery_distance = @request_payload[:max_delivery_distance]
      provider_hash = @request_payload.clone
      provider_hash.store("id", Provider.max_id)
      provider_hash.store("balance", 0)

      if Provider.exists?({'email'=>email}) or 
         Consumer.exists?({'email'=>email})
        halt 409  ## El usuario ya existe
      end
      if email.nil? || location.nil? || store_name.nil?
        halt 400  ## Campos necesarios
      end
      provider = Provider.from_hash(provider_hash)
      provider.update
      provider.id.to_s
    end

    post '/login' do
      hash_response = {}
      email = @request_payload[:email]
      password = @request_payload[:password]
      unless ( Consumer.exists?({'email'=>email}) or 
               Provider.exists?({'email'=>email}) )
        halt 401  ## No existe el usuario
      end
      unless ( Consumer.exists?({'email'=>email, 'password'=>password}) or 
              Provider.exists?({'email'=>email, 'password'=>password}) )
        halt 403  ## Password incorrecto
      end
      isProvider = Provider.exists?({'email'=>email})
      if isProvider
        user = Provider.find_by_email({'email'=>email})
      else
        user = Consumer.find_by_email({'email'=>email})
      end
      session[:logged_id] = user.id
      hash_response.store("id", user.id)
      hash_response.store("isProvider", isProvider)
      hash_response.to_json
    end

    post '/logout' do
      session[:logged_id] = nil
    end

    post '/items' do
      name = @request_payload[:name]
      price = @request_payload[:price].to_f
      provider = @request_payload[:provider].to_i
      model_hash = @request_payload.clone
      model_hash["id"] = Item.max_id
      model_hash["provider"] = provider
      if name.nil? || price.zero? || provider.zero?
        halt 400  ## Falta name | price | provider
      end
      unless Provider.exists?({'id'=>provider})
        halt 404  ## No existe el provider
      end
      if Item.exists?({'name'=>name, 'provider'=>provider})
        halt 409  ## Item duplicado para provider
      end
      item = Item.from_hash(model_hash)
      item.update
      item.id.to_json
    end

    get '/items' do
      provider_id = params[:provider].to_i
      if provider_id.zero? or (
         Provider.exists?({'id'=>provider_id}) and
         ENV["RACK_ENV"].eql?("test") )
        items = Item.all
      else
        unless Provider.exists?({'id'=>provider_id})
          halt 404  ## No existe el provider
        end
        items = Item.filter({"provider"=>provider_id})
      end
      json_response = []
      items.each do |item|
        json_response << item.to_hash
      end
      json json_response
    end

    post '/items/delete/:id' do
      item_id = params[:id].to_i
      unless Item.index?(item_id)
        halt 404  ## No existe la orden
      end
      #item = Item.find(item_id)
      #unless item.provider == session[:logged_id]
        #halt 403  ## No pertenece al provider loggeado
      #end
      Item.delete(item_id)
      Item.save
    end

    get '/users/:id' do
      hash_response = {}
      id = params[:id].to_i
      if id.nil?
        halt 400  ## No se paso :id
      end
      unless Consumer.index?(id) or Provider.index?(id)
        halt 404  ## No existe el usuario
      end
      user = Provider.find(id)
      if user.nil?
        user = Consumer.find(id)
      end
      hash_response.store("email", user.email)
      hash_response.store("balance", user.balance)
      hash_response.to_json
    end

    post '/users/delete/:id' do
      user_id = params[:id].to_i
      unless Consumer.index?(user_id) or Provider.index?(user_id)
        halt 404  ## No existe el usuario
      end
      isProvider = Provider.index?(user_id)
      if isProvider
        Provider.delete(user_id)
        Provider.save
      else
        Consumer.delete(user_id)
        Consumer.save
      end
    end

    post '/orders' do
      items = []
      itemsIds = []
      @request_payload[:items].each do |item|
        hash_item = {}
        hash_item.store("id", item[:id])
        hash_item.store("amount", item[:amount])
        items << hash_item
        itemsIds << item[:id]
      end
      consumer = @request_payload[:consumer]
      provider = @request_payload[:provider]
      model_hash = @request_payload.clone
      model_hash["id"] = Order.max_id
      model_hash["status"] = ORDER_PAYED
      model_hash["items"] = items
      model_hash["rating"] = 0

      if provider.nil? || consumer.nil? || items.empty?
        halt 400  ## Falta provider | consumer | items == []
      end
      unless ( Provider.index?(provider) && Consumer.index?(consumer) && 
        Item.existAll?(itemsIds) )
        halt 404  ## No existe provider | consumer | items
      end
      provider = Provider.find(provider)
      consumer = Consumer.find(consumer)
      order = Order.from_hash(model_hash)
      order.update
      provider.balance += order.order_amount
      consumer.balance -= order.order_amount
      provider.update
      consumer.update
      order.id.to_json
    end

    get '/orders' do
      hash_response = {}
      json_response = []
      user_id = params[:user_id].to_i
      if user_id.zero?
        halt 400  ## No se paso :id
      end
      unless Consumer.index?(user_id) or Provider.index?(user_id)
        halt 404  ## No existe el usuario
      end
      user = Provider.find(user_id)
      if user.nil?
        user = Consumer.find(user_id)
      end
      if user.isProvider
        orders = Order.filter({"provider"=>user_id})
      else
        orders = Order.filter({"consumer"=>user_id})
      end
      orders.each { |order|
        order_hash = {}
        order_hash.store("id", order.id)
        order_hash.store("provider", order.provider)
        order_hash.store("provider_name", Provider.find(order.provider).store_name)
        order_hash.store("consumer", order.consumer)
        order_hash.store("consumer_email", Consumer.find(order.consumer).email)
        order_hash.store("consumer_location", Consumer.find(order.consumer).location)
        order_hash.store("order_amount", order.order_amount)
        order_hash.store("status", order.status)
        order_hash.store("rating", order.rating)
        json_response << order_hash
      } unless orders.nil?
      json json_response
    end

    get '/orders/detail/:id' do
      json_response = []
      order_id = params[:id].to_i
      unless Order.index?(order_id)
        halt 404  ## No existe la orden
      end
      order = Order.find(order_id)
      order.items.each do |order_item|
        item_hash = {}
        item = Item.find(order_item["id"])
        item_hash.store("id", item.id)
        item_hash.store("name", item.name)
        item_hash.store("price", item.price)
        item_hash.store("amount", order_item["amount"])
        json_response << item_hash
      end
      json json_response
    end

    post '/orders/delete/:id' do
      order_id = params[:id].to_i
      unless Order.index?(order_id)
        halt 404  ## No existe la orden
      end
      Order.delete(order_id)
      Order.save
    end

    post '/deliver/:id' do
      order_id = params[:id].to_i
      unless Order.index?(order_id)
        halt 404  ## No existe la orden
      end
      order = Order.find(order_id)
      order.status = ORDER_DELIVERED
      order.update
    end

    post '/finish' do
      order_id = @request_payload[:id]
      rating = @request_payload[:rating]
      unless Order.index?(order_id)
        halt 404  ## No existe la orden
      end
      order = Order.find(order_id)
      order.status = ORDER_FINISHED
      order.rating = rating
      order.update
    end

    get '/providers' do
      location_id = params[:location].to_i
      if location_id.zero?
        providers = Provider.all
      else
        providers = Provider.filter({"location"=>location_id})
      end
      json_response = []
      providers.each do |provider|
        provider_hash = {}
        provider_hash.store("id", provider.id)
        provider_hash.store("email", provider.email)
        provider_hash.store("location", provider.location)
        provider_hash.store("store_name", provider.store_name)
        provider_hash.store("max_delivery_distance", 
                            provider.max_delivery_distance)
        json_response << provider_hash
      end
      json json_response
    end

    get '/consumers' do
      consumers = Consumer.all
      json_response = []
      consumers.each do |consumer|
        consumer_hash = {}
        consumer_hash.store("id", consumer.id)
        consumer_hash.store("email", consumer.email)
        consumer_hash.store("location", consumer.location)
        json_response << consumer_hash
      end
      json json_response
    end

    get '/locations' do
      locations = Location.all
      json_response = []
      locations.each do |location|
        json_response << location.to_hash
      end
      json json_response
    end

    get '*' do
      halt 404
    end
  end

  # This goes last as it is a catch all to redirect to the React Router
  get '/*' do
    erb :index
  end
end
