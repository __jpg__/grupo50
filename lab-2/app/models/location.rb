require './models/model'

class Location < Model
    @db_filename = 'locations.json'

    # Attributes to save data to locations
    attr_reader :name
end