import React, { Component, PropTypes } from 'react';
import Auth from '../modules/Auth';
import axios from 'axios';
import { Redirect } from 'react-router'

class Logout extends Component {
  constructor(props){
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    axios
      .post('/api/logout')
      .then(function (response) {
        if (response.status == 200) {
          Auth.deauthenticateUser();
        }
      })
      .catch(function (error) {
          alert(error)
      });
      this.setState({isLoading: false})
  }

  render() {

    if (this.isLoading){
      return (
        <h1 className="loading-text">
          Logging out...
        </h1>
      );
    }else {
      return (
        <Redirect to="/"/>
      );
    }
  }
}

export default Logout;