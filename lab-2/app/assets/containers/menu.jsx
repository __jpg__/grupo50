import React from 'react';
import axios from 'axios';
import Auth from '../modules/Auth';
import MenuListConsumer from '../presentational/menuListConsumer';


export default class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      total: 0,
      loading: true,
      items: [],
      newItems: []  // array of objects [{id:int, amount:int}]
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event){
    // name is index
    const { name, value } = event.target;
    const nvalue = parseInt(value) 
    const newItems = this.state.newItems;
    const price = this.state.items[name]['price']
    newItems[name]['amount'] = nvalue
    newItems[name]['subtotal'] = nvalue * price
    const subtotals = newItems.map((item) => item.subtotal)
    const total = subtotals.reduce((total, subtotal) => total + subtotal)
    this.setState({
      newItems: newItems,
      total: total
    })
  }

  handleSubmit(event){
    event.preventDefault()

    const newItems = this.state.newItems
    newItems.forEach(item => {
      delete item.subtotal
    })
    const order = {
      provider: this.state.items[0]['provider'],
      items: newItems,
      consumer: parseInt(Auth.getToken())
    }
    this.createApiOrder(order).then(response => {
      this.props.history.push({
        pathname: '/profile',
        state: {}
      })
    })
  }

  createApiOrder(order) {
    return axios.post("/api/orders", order);
  }

  generateArrayOfItems(items){
    const newItems = []
    items.forEach(item => {
      const obj = { id: item.id, amount: 0, subtotal: 0 }
      newItems.push(obj)
    });

    return newItems
  }

  componentDidMount() {
    axios
      .get("/api/items", {params: {provider: this.props.match.params.id}})
      .then(
        response => {
          const newItems = this.generateArrayOfItems(response.data)
          this.setState({items: response.data, newItems: newItems, loading: false})
        }
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  render() {
    return (
      <MenuListConsumer loading={this.state.loading}
          items={this.state.items}
          newItems={this.state.newItems}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          total={this.state.total}
      />
    );
  }
}