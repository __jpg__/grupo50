import React from 'react';
import axios from 'axios';
import Auth from '../modules/Auth';
import { withRouter } from "react-router-dom";
import LoginForm from '../presentational/loginForm';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                email: '',
                password: ''
            },
            isLoading: false,
            isDisabled: true,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        
    }

    validate(email, password) {
        // true means invalid, so our conditions got reversed
        return {
            email: email.length === 0,
            password: password.length === 0,
        };
    }

    canBeSubmitted() {
        const errors = this.validate(this.state.user.email, this.state.user.password);
        const isDisabled = Object.keys(errors).some(x => errors[x]);
        this.setState({ isDisabled: isDisabled });
        return isDisabled;
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        }, () => this.canBeSubmitted());
    }

    handleClick() {
        var payload = {
            "email": this.state.user.email,
            "password": this.state.user.password
        }
        axios
            .post('/api/login', payload)
            .then(function (response) {
                if (response.status == 200) {
                    const isProvider = response.data.isProvider
                    Auth.authenticateUser(response.data.id, isProvider);
                }
            }).catch(function (error) {
                if (error.response.status == 401) {
                    alert("No existe el usuario");
                }
                else {
                    alert("El password es incorrecto");
                }
            });
            this.setState({isLoading: true})
    }

    render() {
        return (
            <LoginForm
                handleChange={this.handleChange}
                handleClick={this.handleClick}
                user={this.state.user}
                isLoading={this.state.isLoading}
                isDisabled={this.state.isDisabled}
            />
        );
    }
}
export default withRouter(Login);