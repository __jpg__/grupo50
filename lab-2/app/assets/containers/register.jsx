import React from 'react';
import axios from 'axios';
import RegisterForm from '../presentational/registerForm'

function validate(email, password) {
    // true means invalid, so our conditions got reversed
    return {
      email: email.length === 0,
      password: password.length === 0,
    };
  }

export default class Register extends React.Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            user: {
                email: '',
                password: '',
                location: 1, // default value
                store_name: '',
                max_delivery_distance: ''
            },
            touched: {
                email: false,
                password: false,
            },
            isLoading: false,
            isDisabled: true,
            locations: []
        };
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        }, () => this.canBeSubmitted());
        
    }

    shouldMarkError = (field) => {
        const errors = validate(this.state.user.email, this.state.user.password);
        const hasError = errors[field];
        const shouldShow = this.state.touched[field];
        
        return hasError ? shouldShow : false;
    };

    handleBlur = (field) => (event) => {
        this.setState({
          touched: { ...this.state.touched, [field]: true },
        });
    }

    handleSubmit(event) {
        if(event.target.id == "consumer"){
            var url = "/api/consumers";
            var payload = {
                email: this.state.user.email,
                password: this.state.user.password,
                location: this.state.user.location
            }
        }else { // "provider"
            url = "/api/providers";
            payload = this.state.user
        }
        this.setState({ isLoading: false }, () => {
            axios
                .post(url, payload
                ).then(function (response) {
                    if (response.status == 200) {
                       alert("Register successfull");
                    }
                }).catch(function (error) {
                    if (error.response.status == 409) {
                        alert("El usuario actualmente existe");
                    }
                    else {
                        alert("Faltan campos requeridos");
                    }
                });
        })
        this.setState({ isLoading: true });
    }

    canBeSubmitted() {
        const errors = validate(this.state.user.email, this.state.user.password);
        const isDisabled = Object.keys(errors).some(x => errors[x]);
        this.setState({isDisabled: isDisabled});
        return isDisabled;
    }

    componentWillMount() {
        axios
            .get("/api/locations")
            .then(
                response => this.setState({ locations: response.data, isLoading: false })
            ).catch(
                error => {
                    if (!error.response)
                        alert(error);
                    else if (error.response.data && error.response.status !== 404)
                        alert(error.response.data);
                    else
                        alert(error.response.statusText);
                    this.setState({ loading: false });
                }
            );
    }

    render() {
        return (
            <RegisterForm
                shouldMarkError={this.shouldMarkError}
                handleSubmit={this.handleSubmit}
                handleChange={this.handleChange}
                handleBlur={this.handleBlur}
                user={this.state.user}
                locations={this.state.locations}
                isLoading={this.state.isLoading}
                isDisabled={this.state.isDisabled}
            />
        );
    }
}