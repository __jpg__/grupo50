import React from 'react';
import Auth from '../modules/Auth';
import ProfileTab from '../presentational/profileTab'
import ProfileContent from '../presentational/profileContent'
import Header from '../presentational/header';
import Footer from '../presentational/footer';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      renderComponent: false
    }

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(event) {
    this.setState({
      renderComponent: !this.state.renderComponent
    });
  }

  render() {
    const isProvider = Auth.isUserProvider();

    return (
      <div className="main-container container-fluid">
        <Header />
        <div className="body">
          <h1>Perfil de Usuario</h1>
          <ProfileTab 
            isProvider={isProvider} 
            handleSelect={this.handleSelect} 
          />
          <ProfileContent />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Profile
