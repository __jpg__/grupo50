import React from 'react';
import axios from 'axios';
import DeliveryList from '../presentational/deliveryList';


export default class Delivery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      providers: []
    };
  }

  componentDidMount() {
    axios
      .get("/api/providers", {params: {location: this.props.location.state.location}})
      .then(
        response => this.setState({providers: response.data, loading: false})
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  render() {
    return (
      <DeliveryList loading={this.state.loading}
          providers={this.state.providers}/>
    );
  }
}