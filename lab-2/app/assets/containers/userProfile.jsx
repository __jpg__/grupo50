import React from 'react';
import axios from 'axios';
import Auth from '../modules/Auth';

class UserProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            balance: 0
        };
    }

    componentWillMount() {
        axios
            .get("/api/users/" + Auth.getToken())
            .then(response => this.setState({
                email: response.data.email,
                balance: response.data.balance
            }))
            .catch(error => {
                if (!error.response)
                    alert(error);
                else if (error.response.data && error.response.status !== 404)
                    alert(error.response.data);
                else
                    alert(error.response.statusText);
            }
            );
    }

    render() {
        return (
            <div className="tab-pane active" id="account">
                <div className="row">
                    <div className="col-sm-12">
                        <ul>
                            <li><strong>Correo Electrónico: </strong>{this.state.email}</li>
                            <li><strong>Balance: </strong>{this.state.balance}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserProfile;