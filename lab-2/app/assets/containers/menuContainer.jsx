import React from 'react';
import axios from 'axios';
import Auth from '../modules/Auth';
import MenuList from '../presentational/menuList';
import MenuItem from '../presentational/menuItem';
import MenuInput from '../presentational/menuInput';
import MenuButton from '../presentational/menuButton';

class MenuContainer extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.state = {
      items: [],
      added: [],
      deleted: [],
      item: {
        name: '',
        price: 0
      },
      loading: true
    };
  }

  isInItems(param_name) {
    // Comprobar si existe actualmente el item
    const names = this.state.items.map((item) => item.name)
    return names.some(name => name == param_name)
  }

  handleDelete(event) {
    const item_name = event.target.title
    const { deleted } = this.state;
    const { items } = this.state;
    const { item } = this.state;

    // Si existe en los items -> agregar a deleted
    // En otro caso lo quito de added
    const index = items.findIndex(item => item.name == item_name)
    const id = items[index]['id']
    items.splice(index, 1)

    // Si el item posee id
    if (typeof id !== "undefined") {
      this.setState({
        deleted: [...deleted, id],
        items: items,
        item: { name: '', price: 0 }
      })
    } else {
      // El item es nuevo y no posee id
      this.setState({
        items: items,
        item: { name: '', price: 0 }
      })
    }
  }

  handleAdd() {
    const { items } = this.state;
    const { item } = this.state;

    // Si el item esta vacio
    if (!item.name) {
      return;
    }
    // Si el item es nuevo lo agregamos
    else if (!this.isInItems(item.name)) {

      const newItem = {
        id: "new",
        name: item.name,
        price: item.price,
        provider: Auth.getToken()
      }

      this.setState({
        items: [...items, newItem],
        item: { name: '', price: 0 }
      })
    } else {
      alert("Ya hay un item con ese nombre");
    }
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { item } = this.state;
    this.setState({
      item: { ...item, [name] : value }
    })
  }

  handleSubmit(event) {
    var requests_list = []
    this.state.items.map((item) => {
      item.price = parseInt(item.price)
      if(item.id == "new"){
        requests_list.push(this.createApiItem(item))
      }
    })
    this.state.deleted.map((item_id) =>
      requests_list.push(this.deleteApiItem(item_id)))

    Promise.all(requests_list).then(responses => {
      this.getApiItems().then(response => {
        this.setState({ items: response.data, deleted: [], loading: false })
      })
    });
  }

  getApiItems() {
    return axios.get("/api/items", {params: {provider: Auth.getToken()}});
  }

  createApiItem(item) {
    return axios.post("/api/items", item);
  }

  deleteApiItem(item_id) {
    return axios.post("/api/items/delete/" + item_id);
  }

  componentWillMount() {
    this.getApiItems()
      .then((response) =>
        this.setState({ items: response.data, loading: false }))
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="body text-center my-3">
          <div className="fa-3x my-3">
            <i className="fas fa-spinner fa-spin" />
          </div>
          <h4>Loading...</h4>
        </div>
      );
    } else {
      return (
        <div className="tab-pane" id="menu" >
          <MenuList>
            {this.state.items.map((item, idx) =>
              <MenuItem
                key={idx}
                item={item}
                handleDelete={this.handleDelete}
              />
            )}
          </MenuList>
          <MenuInput
            item={this.state.item}
            handleChange={this.handleChange}
            handleAdd={this.handleAdd}
          />
          <MenuButton handleSubmit={this.handleSubmit} />
        </div>
      );
    }
  }
}

export default MenuContainer;