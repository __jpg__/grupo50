import React from 'react';
import axios from 'axios';
import Auth from '../modules/Auth';
import PedidoList from '../presentational/pedidoList';
import PedidoItem from '../presentational/pedidoItem';
import MenuInput from '../presentational/menuInput';
import MenuButton from '../presentational/menuButton';

const ORDER_PAYED = 'Pagado'
const ORDER_DELIVERED = 'Entregado'
const ORDER_FINISHED = 'Finalizado'

class PedidoContainer extends React.Component {
  constructor(props) {
    super(props);

    this.handleDeliver = this.handleDeliver.bind(this);
    this.ratingChanged = this.ratingChanged.bind(this);
    this.handleFinish = this.handleFinish.bind(this);

    this.state = {
      orders: [],
      rating: 0
    };
  }

  handleDeliver(event) {
    event.preventDefault()

    const index = event.target.id
    const order_id = this.state.orders[index]['id']
    var orders = this.state.orders
    orders[index]['status'] = ORDER_DELIVERED
    axios.post("/api/deliver/" + order_id).then(response => {
      this.setState({orders: orders})
    })
  }

  handleFinish(event) {
    const index = event.target.id
    const order_id = this.state.orders[index]['id']
    var orders = this.state.orders
    orders[index]['status'] = ORDER_FINISHED
    const order = {
      id: order_id,
      rating: this.state.rating
    }
    axios.post("/api/finish", order).then(response => {
      this.getApiOrders().then(response => {
        this.setState({ orders: response.data })
      })
    })
  }

  ratingChanged(newRating){
    this.state.rating = newRating
  }

  getApiOrders() {
    return axios.get("/api/orders", {params: {user_id: Auth.getToken()}});
  }

  componentWillMount() {
    this.getApiOrders()
      .then((response) =>
        this.setState({ orders: response.data }))
  }

  render() {
    const isProvider = Auth.isUserProvider();

    return (
      <div className="tab-pane" id="order">
        <PedidoList>
          {this.state.orders.map((order, idx) =>
            <PedidoItem
              key={idx}
              index={idx}
              order={order}
              rating={this.state.rating}
              isProvider={isProvider}
              handleDeliver={this.handleDeliver}
              ratingChanged={this.ratingChanged}
              handleFinish={this.handleFinish}
            />
          )}
        </PedidoList>
      </div>
    );
  }
}

export default PedidoContainer;