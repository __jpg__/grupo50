import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './containers/main';
import Profile from './containers/profile';
import Login from './containers/login';
import Logout from './containers/logout';
import Register from './containers/register';
import Delivery from './containers/delivery';
import Menu from './containers/menu';
import NotFound from './presentational/notfound';


export default function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/profile" component={Profile} />
        <Route path="/delivery" component={Delivery} />
        <Route path="/menu/:id" component={Menu} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Register} />
        <Route path="/logout" component={Logout} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}
