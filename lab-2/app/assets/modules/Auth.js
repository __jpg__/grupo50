class Auth {

  /**
   * Authenticate a user. Save a token string in Local Storage
   *
   * @param {string} token
   */
  static authenticateUser(token, isProvider) {
    localStorage.setItem('loggedId', token);
    const type = isProvider ? 'provider' : 'consumer'
    localStorage.setItem('type', type);
  }

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
  static isUserAuthenticated() {
    return localStorage.getItem('loggedId') !== null;
  }

  static isUserConsumer() {
    return localStorage.getItem('type') == 'consumer';
  }

  static isUserProvider() {
    return localStorage.getItem('type') == 'provider';
  }

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  static deauthenticateUser() {
    localStorage.removeItem('loggedId');
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  static getToken() {
    return localStorage.getItem('loggedId');
  }

}

export default Auth;