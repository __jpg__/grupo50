import React from 'react';

export default function HeaderItemsLogout() {
    return(
      <ul className="ml-auto navbar-nav">
        <li className="nav-item"><a className="nav-link" href="/signup">Registrate</a></li>
        <li className="nav-item"><a className="nav-link" href="/login">Iniciá Sesión</a></li>
      </ul>
    )
}