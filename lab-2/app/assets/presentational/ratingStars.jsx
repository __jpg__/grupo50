import React from 'react'
import PropTypes from 'prop-types'
import ReactStars from 'react-stars'

function RatingStars(props) {
  let {rating, ratingChanged, edit } = props;

  return (
    <div className="row">
      <div className="text-center col-sm-12">
        <div className="row justify-content-center">
          <div className="text-center">
            <ReactStars
              count={5}
              value={rating}
              onChange={ratingChanged}
              size={42}
              edit={edit}
              color1={'black'}
              color2={'#ffd700'}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

RatingStars.propTypes = {
  rating: PropTypes.number.isRequired,
  ratingChanged: PropTypes.func.isRequired,
  edit: PropTypes.bool.isRequired
}

export default RatingStars