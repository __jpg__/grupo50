import React from 'react'
import PropTypes from 'prop-types'

const MenuList = ({ children }) => (
    <div>{children}</div>
)

MenuList.propTypes = {
  children: PropTypes.node
}

export default MenuList