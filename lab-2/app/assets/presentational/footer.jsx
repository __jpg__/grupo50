import React from 'react';
import PropTypes from 'prop-types';

export default function Footer(prop) {

    return (
        <footer className="page-footer text-right">
            <p>© 2018 - Paradigmas de Programación FaMAF</p>
        </footer>
    );
}