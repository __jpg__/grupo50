import React from 'react';
import PropTypes from 'prop-types';
import Auth from '../modules/Auth';
import UserProfile from '../containers/userProfile';
import MenuContainer from '../containers/menuContainer';
import PedidoContainer from '../containers/pedidoContainer';

class ProfileContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isProvider = Auth.isUserProvider();
    return (
      <div className="tab-content">
        <UserProfile />
        <PedidoContainer />
        {isProvider ? <MenuContainer /> : null}
      </div>
    );
  }
}

export default ProfileContent