import React from 'react'
import PropTypes from 'prop-types'

function ButtonSendOrder(props) {
  let { index, handleDeliver } = props;

  return (
    <div className="row">
      <div className="text-center col-sm-12">
        <button type="button"
          id={index}
          className="btn btn-success"
          onClick={handleDeliver}
        >
          Pedido Enviado
      </button>
      </div>
    </div>
  );
}

ButtonSendOrder.propTypes = {
  index: PropTypes.number.isRequired,
  handleDeliver: PropTypes.func.isRequired
}

export default ButtonSendOrder