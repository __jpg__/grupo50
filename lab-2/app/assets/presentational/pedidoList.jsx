import React from 'react'
import PropTypes from 'prop-types'

const PedidoList = ({ children }) => (
    <span>
      {children}
    </span>
)

PedidoList.propTypes = {
  children: PropTypes.node
}

export default PedidoList