import React from 'react';
import PropTypes from 'prop-types';
import Header from './header';
import Footer from './footer';

export default function MenuListConsumer(props) {
  let { loading, items, newItems, handleChange, handleSubmit, total } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin" />
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div className="main-container container-fluid">
        <Header></Header>
        <div className="body">
          <h1 className="display-4 text-center my-3">¡Hacé tu pedido!</h1>
          <p className="lead text-center">Este es el menu disponible.</p>
          <hr />
          <form onSubmit={handleSubmit}>
            {items.map((item, idx) => (
              <div key={idx} className="row form-group">
                <div className="col-sm-10 offset-sm-1">
                  <div className="input-group">
                    <div className="input-group-prepend">
                      <span className="input-group-text">{item.name}</span>
                      <span className="input-group-text">${item.price}</span>
                      <span className="input-group-text">×</span>
                    </div>
                    <input type="number"
                      name={idx}
                      value={newItems[idx].amount}
                      onChange={handleChange}
                      className="text-right form-control"
                      style={inputStyle} />
                    <div className="input-group-append">
                      <span className="input-group-text" style={spanStyle}>
                        <strong>Subtotal:  </strong>${newItems[idx].subtotal}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            ))}
            <div className="row form-group">
              <div className="col-sm-4 offset-sm-4">
                <button type="submit"
                  className="btn btn-dark btn-lg btn-block">
                  Pedí tu delivery! (${total})
                </button>
              </div>
            </div>
          </form>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

MenuListConsumer.propTypes = {
  loading: PropTypes.bool.isRequired,
  items: PropTypes.array.isRequired,
  newItems: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired
};

const inputStyle = {
  direction: 'rtl'
};

const spanStyle = {
  minWidth: '10em'
};

