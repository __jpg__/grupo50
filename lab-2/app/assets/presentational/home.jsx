import React from 'react';
import PropTypes from 'prop-types';
import Header from './header';
import Footer from './footer';

function Body(props) {
  let { locations, location, submitForm, handleChange } = props;
  
  return (
    <div className="body">
    <div className="main-form jumbotron">
      <div className="main-div">
        <h1 className="display-3 text-center mb-5">¿Qué vas a comer hoy?</h1>
          <form onSubmit={submitForm} className="">
            <div className="row form-group">
              <label 
                className="text-lg-right col-form-label-lg col-sm-3 col-form-label">
                Elegí tu ubicación actual
              </label>
              <div className="col-sm-6">
                <select name="location" 
                  value={location}
                  onChange={handleChange}
                  className="form-control-lg form-control">
                  { 
                    locations.map((loc, idx) => (
                    <option key={idx} value={loc.id}>{loc.name}</option>
                    ))
                  }
                </select>
              </div>
              <div className="col-sm-3">
                <button type="submit" 
                  className="btn btn-dark btn-lg btn-block">Buscar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
        </div>
  );
}

export default function Home(props) {
  let { loading, locations, location, submitForm, handleChange } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div className="main-container container-fluid">
        <Header></Header>
        <Body locations={locations}
              location={location}
              submitForm={submitForm}
              handleChange={handleChange}
        ></Body>
        <Footer></Footer>
      </div>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool.isRequired,
  locations: PropTypes.array.isRequired,
  location: PropTypes.number.isRequired, 
  submitForm: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired
};
