import React from 'react';
import PropTypes from 'prop-types';
import Header from './header';
import Footer from './footer';

export default function DeliveryList(props) {
  let { loading, providers } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div className="main-container container-fluid">
        <Header></Header>
        <div className="body">
          <h1 className="display-4 text-center my-3">¡Elegí tu Delivery!</h1>
          <p className="lead text-center">Estos son los providers de tu zona.</p>
          <hr/>
          <div className="row">
            <div className="col-sm-10 offset-sm-1">
              <ul className="list-group">
                { providers.map((pro, idx) => (  
                  <a key={idx} 
                    className="list-group-item"  
                    href={"/menu/" + pro.id}>{pro.store_name}</a>))
                }
              </ul>
            </div>
          </div>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

DeliveryList.propTypes = {
  loading: PropTypes.bool.isRequired,
  providers: PropTypes.array.isRequired
};
