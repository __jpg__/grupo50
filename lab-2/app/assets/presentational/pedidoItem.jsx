import React from 'react'
import PropTypes from 'prop-types'
import RatingStars from './ratingStars'
import ButtonQualify from './buttonQualify'
import ButtonSendOrder from './buttonSendOrder'

const ORDER_PAYED = 'Pagado'
const ORDER_DELIVERED = 'Entregado'
const ORDER_FINISHED = 'Finalizado'

function PedidoItem(props) {
  let {
    index, 
    order,
    isProvider, 
    handleDeliver,
    ratingChanged,
    handleFinish } = props;

  var orderComponent = ""
  const name = isProvider ? order.provider_name : order.consumer_email
  const price = order.order_amount
  const status = order.status
  const rating = order.rating

  const caseOne = (status == ORDER_PAYED) && isProvider
  const caseTwo = (status == ORDER_PAYED) && !isProvider
  const caseThree = (status == ORDER_DELIVERED) && isProvider
  const caseFour = (status == ORDER_DELIVERED) && !isProvider
  const caseFive = (status == ORDER_FINISHED) && isProvider
  const caseSix = (status == ORDER_FINISHED) && !isProvider

  if(caseOne){
    orderComponent = (
      <ButtonSendOrder
        index={index}
        handleDeliver={handleDeliver}
      />
    )
  } else if (caseTwo){
    orderComponent = (
      <div>
        <RatingStars
          rating={rating}
          ratingChanged={ratingChanged}
          edit={false}
        />
        <ButtonQualify
          index={index}
          handleFinish={handleFinish}
          isDisabled={true}/>
      </div>
    )
  } else if (caseThree || caseFive || caseSix){
    orderComponent = (
      <RatingStars
        rating={rating}
        ratingChanged={ratingChanged}
        edit={false}
      />
    )
  } else if (caseFour) {
    orderComponent = (
      <div>
        <RatingStars
          rating={rating}
          ratingChanged={ratingChanged}
          edit={true}
        />
        <ButtonQualify
          index={index}
          handleFinish={handleFinish}
          isDisabled={false}
        />
      </div>
    )
  }

  return (
    <div className="my-3 row">
      <div className="col-sm-8 offset-sm-2">
        <div className="w-100 card">
          <h4 className="card-header">
            <strong>
              {name}: ${price}
            </strong>
            <span className="float-right">
              <span className="badge badge-primary">{status}</span>
            </span>
          </h4>
          <div className="card-body">
            {orderComponent}
          </div>
        </div>
      </div>
    </div>
  );
}

PedidoItem.propTypes = {
  index: PropTypes.number.isRequired,
  order: PropTypes.object.isRequired,
  isProvider: PropTypes.bool.isRequired,
  handleDeliver: PropTypes.func.isRequired, 
  ratingChanged: PropTypes.func.isRequired,
  handleFinish: PropTypes.func.isRequired
}

export default PedidoItem