import React from 'react';
import PropTypes from 'prop-types';
import Auth from '../modules/Auth';
import HeaderItemsLogin from './headerItemLogin';
import HeaderItemsLogout from './headerItemLogout';

export default function Header(props) {
        const isLoggedIn = Auth.isUserAuthenticated();
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark">
                    <a className="navbar-brand" href="/">Deliveru</a>
                    { isLoggedIn 
                        ? <HeaderItemsLogin/>
                        : <HeaderItemsLogout/> 
                    }
                </nav>
            </header>
        )
}