import React from 'react'
import PropTypes from 'prop-types'

function ButtonQualify(props) {
  let { index, handleFinish, isDisabled } = props;

  return (
    <div className="row">
      <div className="text-center col-sm-4 offset-sm-4">
        <button type="submit" 
          id={index}
          className="btn btn-dark"
          disabled={isDisabled}
          onClick={handleFinish}
        > Calificar
        </button>
      </div>
    </div>
  );
}

ButtonQualify.propTypes = {
  index: PropTypes.number.isRequired,
  handleFinish: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired
}

export default ButtonQualify