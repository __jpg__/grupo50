import React from 'react'
import PropTypes from 'prop-types'

const MenuButton = ({ handleSubmit }) => (
  <div className="row form-group">
    <div className="col-sm-4 offset-sm-4">
      <button type="submit" 
          className="btn btn-dark btn-lg btn-block"
          onClick={handleSubmit}
      >
        ¡Cambiá tu menú!
      </button>
    </div>
  </div>
)

MenuButton.propTypes = {
  handleSubmit: PropTypes.func.isRequired
}

export default MenuButton