import React from 'react'
import PropTypes from 'prop-types'

const priceStyle = {
  textAlign: 'right',
};

const MenuItem = ({ item, handleDelete }) => (
  <div className="row form-group">
    <div className="col-sm-10 offset-sm-1">
      <div className="input-group">
        <div className="input-group-prepend">
          <button type="button"
            className="btn btn-danger"
            onClick={handleDelete}
          >
            <i className="fas fa-times" title={item.name}></i>
          </button>
        </div>
        <div className="input-group-append">
          <span className="input-group-text" id="menu_item_name">{item.name}</span>
          <span className="input-group-text" style={priceStyle}>$ {item.price}</span>
        </div>
      </div>
    </div>
  </div>
)

MenuItem.propTypes = {
  item: PropTypes.object.isRequired,
  handleDelete: PropTypes.func.isRequired
}

export default MenuItem