import React from 'react';
import PropTypes from 'prop-types';

function ProfileTab(props) {
  let { isProvider, handleSelect } = props

  var liElement;
  if (isProvider) {
    liElement = (
      <li className="nav-item">
        <a className="nav-link" data-toggle="tab" href="#menu">
          Menu
        </a>
      </li>);
  }

  return (
    <ul className="my-3 nav nav-tabs">
      <li className="nav-item">
        <a className="active nav-link" 
          data-toggle="tab" 
          href="#account"
        >
          Datos de Cuenta
        </a>
      </li>
      <li  className="nav-item">
        <a className="nav-link" 
          data-toggle="tab" 
          href="#order"
          onClick={handleSelect}
        >
          Pedidos
        </a>
      </li>
      {liElement}
    </ul>
  );
}

ProfileTab.propTypes = {
  isProvider: PropTypes.bool.isRequired,
  handleSelect: PropTypes.func.isRequired
};

export default ProfileTab