import React from 'react'
import PropTypes from 'prop-types'

const textStyle = {
  minWidth: '75%',
};

const numberStyle = {
  direction: 'rtl',
};

const MenuInput = ({ item, handleChange, handleAdd }) => (
  <div className="row form-group">
    <div className="col-sm-10 offset-sm-1">
      <div className="input-group">
        <div className="input-group-prepend">
          <button type="button"
            className="btn btn-success"
            onClick={handleAdd}
          >
            <i className="fas fa-plus"></i>
          </button>
        </div>
        <input type="text"
          name="name"
          value={item.name}
          onChange={handleChange}
          className="form-control"
          style={textStyle}
        />
        <div className="input-group-prepend">
          <span className="input-group-text"> $ </span>
        </div>
        <input type="number"
          step="0.5"
          name="price"
          value={item.price}
          onChange={handleChange}
          className="text-right form-control"
          style={numberStyle}
        />
      </div>
    </div>
  </div>
)

MenuInput.propTypes = {
  item: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleAdd: PropTypes.func.isRequired
}

export default MenuInput