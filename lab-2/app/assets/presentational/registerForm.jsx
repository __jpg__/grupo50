import React, { PropTypes } from 'react';
import Header from '../presentational/header';
import Footer from '../presentational/footer';

const RegisterForm = ({
  shouldMarkError,
  handleSubmit,
  handleChange,
  handleBlur,
  user,
  locations,
  isLoading,
  isDisabled
}) => (
    <div className="main-container container-fluid">
      <Header />
      <div className="body">
        <h1 className="display-4 text-center my-3">¡Registrate ahora!</h1>
        <p className="lead text-center">¡Se parte de la comunidad! Puedes registrarte como usuario para hacer tus pedidos o como servicio de delivery.</p>

        <ul className="my-3 nav nav-tabs">
          <li className="nav-item"><a className="active nav-link" data-toggle="tab" href="#consumer">Registro de usuarios</a></li>
          <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#provider">Registro de deliveries</a></li>
        </ul>
        <div className="tab-content">
          <div id="consumer" className="tab-pane active">
            <div className="row">
              <div className="col-sm-12">
                <form className="">
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Correo Electrónico</label>
                    <div className="col-sm-5">
                      <input className={"form-control-lg form-control " + (shouldMarkError('email') ? "is-invalid" : "")}
                        type="email"
                        name="email"
                        id="email-consumer"
                        value={user.email}
                        onChange={handleChange}
                        onBlur={handleBlur('email')}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Contraseña</label>
                    <div className="col-sm-5">
                      <input className={"form-control-lg form-control " + (shouldMarkError('password') ? "is-invalid" : "")}
                        type="password"
                        name="password"
                        id="password-consumer"
                        value={user.password}
                        onChange={handleChange}
                        onBlur={handleBlur('password')}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Ubicación</label>
                    <div className="col-sm-5">
                      <select name="location"
                        value={user.location}
                        onChange={handleChange}
                        type="select"
                        className="form-control-lg form-control">
                        {
                          locations.map((loc, idx) => (
                            <option key={idx} value={loc.id}>{loc.name}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-4 offset-sm-4">
                      <button
                        id="consumer"
                        disabled={isDisabled}
                        className="btn btn-dark btn-lg btn-block"
                        onClick={!isLoading ? handleSubmit : null}
                      >       {isLoading ? 'Registrando...' : 'Registrarse'}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div id="provider" className="tab-pane">
            <div className="row">
              <div className="col-sm-12">
                <form className="">
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Nombre del Delivery</label>
                    <div className="col-sm-5">
                      <input className="form-control-lg form-control"
                        type="text"
                        name="store_name"
                        id="store_name-provider"
                        value={user.store_name}
                        onChange={handleChange}
                        onBlur={handleBlur('store_name')}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Correo Electrónico</label>
                    <div className="col-sm-5">
                      <input className={"form-control-lg form-control " + (shouldMarkError('email') ? "is-invalid" : "")}
                        type="email"
                        name="email"
                        id="email-provider"
                        value={user.email}
                        onChange={handleChange}
                        onBlur={handleBlur('email')}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Contraseña</label>
                    <div className="col-sm-5">
                      <input className={"form-control-lg form-control " + (shouldMarkError('password') ? "is-invalid" : "")}
                        type="password"
                        name="password"
                        id="password-provider"
                        value={user.password}
                        onChange={handleChange}
                        onBlur={handleBlur('password')}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Ubicación</label>
                    <div className="col-sm-5">
                      <select name="location"
                        value={user.locations}
                        onChange={handleChange}
                        type="select"
                        className="form-control-lg form-control">
                        {
                          locations.map((loc, idx) => (
                            <option key={idx} value={loc.id}>{loc.name}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                  <div className="row form-group">
                    <label className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Máximo nro de cuadras</label>
                    <div className="col-sm-5">
                      <input className="form-control-lg form-control"
                        style={numStyle}
                        type="number"
                        name="max_delivery_distance"
                        id="max_delivery_distance-provider"
                        value={user.max_delivery_distance}
                        onChange={handleChange}
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-4 offset-sm-4">
                      <button
                        id="provider"
                        disabled={isDisabled}
                        className="btn btn-dark btn-lg btn-block"
                        onClick={!isLoading ? handleSubmit : null}
                      >       {isLoading ? 'Registrando...' : 'Registrarse'}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );

RegisterForm.propTypes = {
  shouldMarkError: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  locations: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool.isRequired
};

const numStyle = {
  textAlign: 'right',
  direction: 'rtl'
};

export default RegisterForm;