import React from 'react';

export default function HeaderItemsLogin() {
    return(
      <ul className="ml-auto navbar-nav">
        <li className="nav-item"><a className="nav-link" href="/profile">Perfil</a></li>
        <li className="nav-item"><a className="nav-link" href="/logout">Cerra Sesión</a></li>
      </ul>
    )
}