import React, { PropTypes } from 'react';
import { Redirect } from 'react-router'
import Header from '../presentational/header';
import Footer from '../presentational/footer';
import Auth from '../modules/Auth';

function LoginForm(props) {
  let { handleChange,
        handleClick,
        user,
        isLoading,
        isDisabled
  } = props

  const isLoggedIn = Auth.isUserAuthenticated();
  const isConsumer = Auth.isUserConsumer();
  const isProvider = Auth.isUserProvider();

  if (isLoggedIn && isConsumer) {
    return (
      <Redirect to="/"/>
    );
  } else if (isLoggedIn && isProvider){
    return (
      <Redirect to="/profile"/>
    );
  } else { 
    return (
    <div className="main-container container-fluid">
      <Header />
      <div className="body">
        <h1 className="display-4 text-center my-3">¡Iniciá Sesión!</h1>
        <p className="lead text-center">Iniciá sesión con el correo electrónico que usaste al <a href="/signup">registrarte.</a></p>
        <hr />
        <div className="row">
          <div className="col-sm-12">
            <form>
              <div className="row form-group">
                <label required className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Correo Electrónico</label>
                <div className="col-sm-5">
                  <input type="email" name="email" id="email"
                    value={user.email}
                    onChange={handleChange}
                    required className="form-control-lg form-control" />
                </div>
              </div>
              <div className="row form-group">
                <label required className="col-form-label-lg col-sm-3 offset-sm-2 col-form-label">Contraseña</label>
                <div className="col-sm-5">
                  <input type="password" name="password" id="password"
                    value={user.password}
                    onChange={handleChange}
                    required className="form-control-lg form-control" />
                </div>
              </div>
              <div className="row form-group">
                <div className="col-sm-4 offset-sm-4">
                  <button
                    disabled={isDisabled}
                    className="btn btn-dark btn-lg btn-block"
                    onClick={!user.isLoading ? handleClick : null}
                  >
                    {user.isLoading ? 'Iniciando...' : 'Iniciar Sesion'}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </div>
    );
  }
}

LoginForm.propTypes = {
  handleChange: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool.isRequired
};

export default LoginForm;