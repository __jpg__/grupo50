# Laboratorio 2 - Paradigmas 2018

## Programaci�n con Frameworks

# Descripcion General de Dise�o

Para modelar este proyecto se desarrollaron componentes jsx que manejan estado interno e interactuan tanto con el usuario como con la API desarrollada en ruby. De esta manera el estado de los componentes va transform�ndose y reflejandolo en la interface gr�fica.

A continuaci�n una descripci�n mas detalla de los componentes definidos, sus roles dentro de la aplicaci�n y los atributos y m�todos que las constituyen:

# Listado de Componentes

# 1. **App.jsx**

Este es el componente principal que al mismo tiempo act�a como enrutador entre los diferentes componentes de la aplicaci�n.



# 2. **Main.jsx**

Esta componente se encarga de construir la pagina principal de la aplicaci�n. Su principal tarea es hacer una llamada a la API para conseguir el listado de **locations** en los que el usuario podr� hacer su pedido

## Estado interno

    this.state  = {
	    loading:  bool	// mutex
	    location:  int	// ubicacion por defecto
	    locations: []	// listado ubicaciones
	}

## Funciones 

    submitForm(event) : Redireccion al elegir ubicacion
    handleChange(event) : Maneja el cambio de ubicacion}
    componentDidMount() : Carga ubicaciones luego de montado


# 3. **Profile.jsx**

Este componente renderiza el perfil adecuando seg�n el tipo de usuario que este registrado en el momento en el sistema


## 3.1 ProfileTab.jsx
Dentro del perfil de usuario, este componente se encarga particularmente de construir el Tab horizontal de opciones.

## 3.2 ProfileContent.jsx
Dentro del perfil de usuario, este componente se encarga particularmente de construir el contenido correspondiente al Tab que se a seleccionado



# 4. **Delivery.jsx**

Este componente se encarga de listar todos los proveedores disponibles para la **location** seleccionada en la b�squeda.

## Estado interno

	this.state  = {
	    loading:  bool	// mutex
	    providers: []	// listado de proveedores
    };

## Funciones 

    componentDidMount() : Carga proveedores luego de montado

# 5. **Menu.jsx**

Este componente le presenta al consumidor el listado de menues disponibles para que el mismo construya su **orden**

## Estado interno

    this.state  = {
	    total: int		// monto total de la orden
	    loading: bool	// mutex
	    items: []		// listado de menues
	    newItems: []	// listado nuevos menues


## Funciones

    handleChange(event) : Modificaciones del usuario
    handleSubmit(event) : Crea la nueva orden
    componentDidMount() : Carga menues luego de montado

# 6. **Login.jsx**

Este componente se encarga del manejo de autenticacion de los usuarios del sistema.

## Estado interno

    this.state  = {
	    user: {
		    email:
		    password:
		}
		isLoading :	// mutex
		isDisabled:	// deshabilita boton submit
	}

## Funciones

    handleChange(event) : Modificaciones del usuario
    handleClick() : Tratamiento de autenticacion

# 7. **Register.jsx**

Este componente se encarga del registro de los nuevos usuarios al sistema

## Estado interno

    this.state  = {
	    user: {
		    email:
		    password:
		    location:
		    store_name:
		    max_delivery_distance:
		}
		touched: {
			email:
			password:
		}
		isLoading:
		isDisabled:
		locations:
	}

## Funciones

    handleChange(event) : Modificaciones del usuario
    handleSubmit(event) : Se crea un nuevo usuario
    componentWillMount() : Carga ubicaciones antes del montado

# 8. **Logout.jsx**

Se encarga de bajar la sesi�n actualmente en curso

