# Laboratorio 1 - Paradigmas 2018

## Programaci�n Orientada a Objetos con Ruby

# Descripcion General de Dise�o

Para modelar este proyecto se desarrollaron clases ruby que heredan de una clase abstracta principal el comportamiento general que todas ellas comparten, principalmente el movimiento de las representaciones de instancias de archivos JSON a instancias representadas por tablas hash.

A continuacion una descripcion mas detalla de las clases definidas, sus roles dentro de la aplicacion y los atributos y metodos que las constituyen:

# Listado de Clases

# 1. **User.rb**

Esta clase hereda los comportamientos de **model.rb** y brinda acceso a los
atributos y metodos compartidos para las dos clases hijas **Provider** y
**Consumers**

## Atributos

`attr_reader :email, :location, :password`  solo lectura
`attr_accessor :balance`                    lectura y escritura

## Metodos

`self.max_id` Metodo de clase para acceder al maximo id compartido
Nota: Requerido para cuando se crean las intancias usando `from_hash`
`self.find_by_email(values)` Metodo de clase que localiza la instancia 
que se corresponda con el email pasado
`isProvider` Metodo de instancia que comprueba si la instancia actual
es del tipo Provider

# 2. **Provider**

Esta clase hereda comportamientos ya descriptos en la clase **User**
y se extiende con nuevos atributos particulares. Su rol es representar
al tipo de usuario proveedor y por ello incorpora nuevas caracteristicas
como un nombre de negocio y una distancia maxima de envio de pedidos.

## Atributos

`@db_filename = 'provider.json'` archivo de respaldo en disco para las 
instancias creadas para esta clase
`attr_reader :store_name, :max_delivery_distance`   solo lectura

# 3. **Consumer**

Esta clase hereda comportamientos ya descriptos en la clase **User**
Su rol es representar al tipo de usuario consumidor dentro de nuestra
aplicacion. Sus caracteristicas son identicas a la de un **User**

## Atributos

`@db_filename = 'consumer.json'` archivo de respaldo en disco para las 
instancias creadas para esta clase

# 4. **Item**

Esta clase hereda las funcionalidades de la clase model y su rol particular
sera representar los productos que los proveedores ofreceran a los 
clientes dentro de la aplicacion. Los mismos solo pueden ser creados por
los usuarios de tipo **Provider** y quedaran asociados a ellos para cuando
se efectue alguna compra.

## Atributos

`@db_filename = 'items.json'` archivo de respaldo en disco para las 
instancias creadas para esta clase
`attr_reader :name, :price, :provider` solo lectura

## Metodos

`self.max_id` Metodo de clase para acceder al maximo id compartido
`self.existAll?(items)` Metodo de clase auxiliar para comprobacion
de existencia de items

# 5. **Order**

Esta clase hereda las funcionalidades de la clase model y su rol particular
sera representar la concresion de una eleccion de items por parte del 
usuario **Consumer** para su compra.

## Atributos

`@db_filename = 'orders.json'` archivo de respaldo en disco para las 
instancias creadas para esta clase
`attr_reader :provider, :items, :consumer` solo lectura
`attr_accessor :status` lectura y escritura

## Metodos

`self.max_id` Metodo de clase para acceder al maximo id compartido
`order_amount` Metodo de instancia auxiliar para calcular el monto
de una instancia orden.

# 6. **Location**

Esta clase hereda las funcionalidades de la clase model y su rol particular
sera representar las ubicaciones atraves de las cuales los proveedores
estaran disponibles y los consumidores requeriran su pedido.

## Atributos

`@db_filename = 'locations.json'` archivo de respaldo en disco para las 
instancias creadas para esta clase
`attr_reader :name` solo lectura
