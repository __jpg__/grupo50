require './models/model'

class Orders < Model
    @db_filename = 'orders.json'

    # Attributes to save data to locations
    attr_accessor :status
    attr_reader :provider, :items, :consumer

    # Definicion de metodo de clase
    def self.max_id
        @@max_id
    end

    # Definicion de metodo de instancia
    def order_amount
        order_amount = 0
        self.items.each do |item|
            order_amount += item["amount"]
        end
        order_amount
    end
end