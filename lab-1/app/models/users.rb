require './models/model'

class Users < Model
    @db_filename = 'users.json'

    # Attributes to save data to users
    attr_reader :email, :location, :password, :isProvider,
    :store_name, :max_delivery_distance, :balance

    def self.max_id
        @@max_id
    end

    def validate_hash(model_hash)
        super
        model_hash.key?('name')
    end

    def self.find_by_email(values)
        self.filter(values).first
    end
end