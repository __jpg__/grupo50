require './models/model'

class Order < Model
    @db_filename = 'orders.json'

    # Attributes to save data to locations
    attr_accessor :status
    attr_reader :provider, :items, :consumer

    # Definicion de metodo de clase
    def self.max_id
        @@max_id
    end

    # Definicion de metodo de instancia
    def order_amount
        order_amount = 0
        self.items.each do |item|
            item_o = Item.find(item["id"])
            if item_o.nil?
                price = 0
            else
                price = item_o.price
            end
            order_amount += item["amount"] * price
        end
        order_amount
    end
end