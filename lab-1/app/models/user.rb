require './models/model'

class User < Model
    @db_filename = 'users.json'

    # Attributes to save data to users
    attr_reader :email, :location, :password
    attr_accessor :balance

    def self.max_id
        @@max_id
    end

    def self.find_by_email(values)
        self.filter(values).first
    end

    def isProvider
        self.class.name.eql?("Provider")
    end
end

class Provider < User
    @db_filename = 'provider.json'

    # Attributes to save data to providers
    attr_reader :store_name, :max_delivery_distance
    
end

class Consumer < User
    @db_filename = 'consumer.json'
end