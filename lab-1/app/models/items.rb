require './models/model'

class Items < Model
    @db_filename = 'items.json'

    # Attributes to save data to consumers
    attr_reader :name, :price, :provider

    def self.max_id
        @@max_id
    end

    # Function to check if exists all items in the list
    # The parameter items is a list from Items
    def self.existAll?(items)
        exist = true
        items.each do |item|
            exist &= self.index?(item)
        end
        exist
    end
end