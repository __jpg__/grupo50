require './models/model'

class Locations < Model
    @db_filename = 'locations.json'

    # Attributes to save data to locations
    attr_reader :name

    def validate_hash(model_hash)
        super
        model_hash.key?('name')
    end
end