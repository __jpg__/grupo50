open GraphicsIntf
open Graphics

module App (Gr: GRAPHICS) = struct
  open Gr
  (* We make an alias of the original Graphics module of OCaml
     in case we need to access it *)
  module G = Graphics

  type tareas = Inicial
    | Espera_circulo
    | Dibuja_circulo
    | Espera_rectangulo
    | Dibuja_rectangulo
    | Espera_borrar
    | Borra
    | Espera_seleccion_1
    | Espera_seleccion_2
    | Selecciona
    | Crecer_seleccion
    | Decrecer_seleccion
    | Exit;;

  type circulo = {x : int; y : int; r : int};;
  type rectangulo = {x : int; y : int; b : int; a: int};;
  type figura = Circulo of circulo | Rectangulo of rectangulo;;
  type seleccion = {x1 : int; y1: int; x2 : int; y2 : int};;
  type state = {figuras: figura list; tarea: tareas; exit: bool; sel: seleccion};;
  
  let seleccion = {x1=0; y1=0; x2=0; y2=0};;
  let state = {figuras=[]; tarea=Inicial; exit=false; sel=seleccion};;

  let f_init () = 
    open_graph "Coreldro"

  let f_end () = 
    close_graph ();
    print_string "Hasta la Victoria...Siempre"; 
    print_newline();;

  let f_mouse s x y =
    (match s.tarea with
      Dibuja_circulo -> 
        G.set_color black;
        fill_circle x y 5;
        let figura = {x=x; y=y; r=5} in
        let newList = List.append (s.figuras) [Circulo figura] in
        {figuras=newList; tarea=Dibuja_circulo; exit=s.exit; sel=s.sel}

      | Dibuja_rectangulo ->
          G.set_color black; 
          fill_rect x y 10 10;
          let figura = {x=x; y=y; b=10; a=10} in
          let newList = List.append (s.figuras) [Rectangulo figura] in
          {figuras=newList; tarea=Dibuja_rectangulo; exit=s.exit; sel=s.sel}
      | Borra -> 
          fill_circle x y 5;
          {figuras=s.figuras; tarea=Borra; exit=s.exit; sel=s.sel}
      | Espera_seleccion_1 ->
          draw_string "Waiting for 2nd point of selection";
          G.set_color black;
          let seleccion = {x1=x; y1=y; x2=0; y2=0} in
          {figuras=s.figuras; tarea=Espera_seleccion_2; exit=s.exit; sel=seleccion}
      | Espera_seleccion_2 ->
          (* Si el rango de seleccion es valido *)
          if s.sel.x1 < x && s.sel.y1 < y then
            begin
              (* Guardo el area seleccionada *)
              draw_string "Selection Saved";
              G.set_color black;
              let seleccion={x1=s.sel.x1; x2=x ;y1=s.sel.y1; y2=y} in
              {figuras=s.figuras; tarea=Selecciona; exit=s.exit; sel=seleccion}
            end
          else begin
            (* vuelvo al estado Espera_seleccion_1 *)
            draw_string "Waiting for 1st point of selection";
            G.set_color black;
            {figuras=s.figuras; tarea=Espera_seleccion_1; exit=s.exit; sel=s.sel}
          end

      | _ -> s
    );;

  let incrementar fig = match fig with
    Circulo fig -> Circulo {x=fig.x; y=fig.y; r=fig.r+2}
    | Rectangulo fig -> Rectangulo {x=fig.x; y=fig.y; b=fig.b+4; a=fig.a+4};;

  let decrementar fig = match fig with
    Circulo fig -> Circulo {x=fig.x; y=fig.y; r=fig.r-2} 
    | Rectangulo fig -> Rectangulo {x=fig.x; y=fig.y; b=fig.b-4; a=fig.a-4};;

  let dibujar fig = match fig with
    Circulo fig -> fill_circle fig.x fig.y fig.r
    | Rectangulo fig -> fill_rect fig.x fig.y fig.b fig.a;;
  
  let f_key s c = 
    (match c with 
      'c' ->  draw_string "Waiting for circle";
              G.set_color black;
              {figuras=s.figuras; tarea=Dibuja_circulo; exit=s.exit; sel=s.sel}
      | 'r' -> draw_string "Waiting for rectangle";
                G.set_color black;
                {figuras=s.figuras; tarea=Dibuja_rectangulo; exit=s.exit; sel=s.sel}
      | 'd' -> draw_string "Waiting for deletion";
                G.set_color white;
               {figuras=s.figuras; tarea=Borra; exit=s.exit; sel=s.sel}
      | 's' -> draw_string "Waiting for 1st point of selection";
                G.set_color black;
                {figuras=s.figuras; tarea=Espera_seleccion_1; exit=s.exit; sel=s.sel}
      | '+' ->  clear_graph ();
                G.set_color black;
                let newList = List.map incrementar s.figuras in
                List.iter dibujar newList;
                {figuras=newList; tarea=Crecer_seleccion; exit=false; sel=s.sel}
      | '-' -> clear_graph ();
                G.set_color black;
                let newList = List.map decrementar s.figuras in
                List.iter dibujar newList;
                {figuras=newList; tarea=Decrecer_seleccion; exit=false; sel=s.sel}
      | 'e' ->  {figuras=s.figuras; tarea=Exit; exit=true; sel=s.sel}
      | _ -> clear_graph (); s);;
  

  let rec coreldro state =
    if state.exit then f_end ();
    let s = G.wait_next_event [button_down; key_pressed] in 
      if s.keypressed then 
        coreldro (f_key state s.key)
      else if s.button then 
        coreldro (f_mouse state s.mouse_x s.mouse_y);;

  let start () =
    try
      f_init ();
      coreldro state
    with Graphic_failure _->();;
end
